<?php

/**
 * @file
 * Contains eventbrite_attendees.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function eventbrite_attendees_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the eventbrite_attendees module.
    case 'help.page.eventbrite_attendees':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('List event attendees and optionally create site accoutns for them') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_cron().
 */
function eventbrite_attendees_cron() {
  eventbrite_attendees_sync_new(TRUE);
}

/**
 * @return string
 */
function eventbrite_attendees_sync_new($update_last_fetch = FALSE) {
  $now = gmdate('Y-m-d\TH:i:s\Z');

  $since = Drupal::state()->get('eventbrite_attendees:last_fetch', '');

  /** @var \Drupal\eventbrite_attendees\ApiInterface $api */
  $api = \Drupal::service('eventbrite_attendees.api');
  $list = $api->attendeesFetch($since);

  \Drupal::logger('eventbrite_attendees')->notice('Fetching data since "@since"', array('@since' => $since));
  // Only update the timestamp if we got some data back, since
  // errors could also give use an empty result.
  if (count($list) > 0) {
    // Update the list before updating the fetch time in case of SQL errors.
    eventbrite_attendees_update_data($list);
    if ($update_last_fetch) {
      $since = $now;
      Drupal::state()->set('eventbrite_attendees:last_fetch', $now);
      $message = 'Added/updated @count attendees and setting new fetch time "@since"';
    }
    else {
      $message = 'Added/updated @count attendees. Not setting last fetch time';
    }
    \Drupal::logger('eventbrite_attendees')->notice($message, array(
      '@count' => count($list),
      '@since' => $since
    ));
  }
  return $since;
}

/**
 * Callback to insert/update attendees with API data.
 */
function eventbrite_attendees_update_data(\Drupal\eventbrite_attendees\AttendeeList $list) {
  $fields = $list->fields();
  foreach ($list as $attendee) {
    $existing_answers = [];
    $existing = \Drupal::database()->query('SELECT 1 FROM {eventbrite_attendees} WHERE id = :id', ['id' => $attendee->id])->fetchField();
    $primary_keys = ['id' => $attendee->id];
    if ($existing) {
      $existing_answers = \Drupal::database()->query('SELECT question_id, 1 FROM {eventbrite_attendee_answers} WHERE attendee_id = :id', array('id' => $attendee->id))->fetchAllKeyed();
    }
    $values = array_flip($fields);
    foreach ($fields as $key) {
      $values[$key] = $attendee->{$key};
    }
    \Drupal::database()->merge('eventbrite_attendees')
      ->keys($primary_keys)
      ->fields($values)
      ->execute();
    foreach ($attendee->answers as $question_id => $answer) {
      $primary_keys = ['attendee_id' => $attendee->id, 'question_id' => $question_id];
      $values = [
        'answer' => $answer->answer,
        'question' => $answer->question,
        'type' => $answer->type,
      ];
      \Drupal::database()->merge('eventbrite_attendee_answers')
        ->keys($primary_keys)
        ->fields($values)
        ->execute();
      // Clear this id from the list.
      unset($existing_answers[$question_id]);
    }
    if ($existing_answers) {
      // Wipe all left-over existing answers since they no longer appear,
      // rather than changing value, when unchecked or omitted.
      \Drupal::database()->delete('eventbrite_attendee_answers')
        ->condition('attendee_id', $attendee->id)
        ->condition('question_id', array_keys($existing_answers), 'IN')
        ->execute();
    }
  }
  \Drupal::service('cache_tags.invalidator')->invalidateTags(['eventbrite_attendees:list']);
}

/**
 * Load all attendee data and answers into an array keyed by ID.
 *
 * @return \Drupal\eventbrite_attendees\AttendeeList
 */
function eventbrite_attendees_load($event_id) {
  $query = \Drupal::database()->select('eventbrite_attendees', 'ea');
  $query->fields('ea');
  $query->condition('ea.event_id', $event_id);
  $query->orderBy('ea.first_name');
  $query->orderBy('ea.last_name');
  $data = $query->execute()->fetchAllAssoc('id');
  foreach ($data as $id => $a) {
    $a->answers = array();
  }
  $query = \Drupal::database()->select('eventbrite_attendee_answers', 'eaa');
  $query->fields('eaa');
  $query->join('eventbrite_attendees', 'ea', 'ea.id = eaa.attendee_id AND ea.event_id = :event_id', [':event_id' => $event_id]);
  $answers = $query->execute()->fetchAll();
  foreach ($answers as $ans) {
    if (isset($data[$ans->attendee_id])) {
      $data[$ans->attendee_id]->answers[$ans->question_id] = $ans;
    }
  }
  return new \Drupal\eventbrite_attendees\AttendeeList($data, 'attending');
}
