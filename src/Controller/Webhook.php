<?php

namespace Drupal\eventbrite_attendees\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\eventbrite_attendees\ApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AttendeeListController.
 */
class Webhook implements ContainerInjectionInterface  {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\eventbrite_attendees\ApiInterface
   */
  protected $api;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * AttendeeListController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory
   * @param \Drupal\eventbrite_attendees\ApiInterface $api
   *   The eventbrite API
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ConfigFactory $config_factory, ApiInterface $api, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config_factory->get('eventbrite_attendees.settings');
    $this->api = $api;
    $this->logger = $logger_factory->get('eventbrite_attendees');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('eventbrite_attendees.api'),
      $container->get('logger.factory')
    );
  }

  /**
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result
   */
  public function access($token) {
    $event_id = $this->config->get('event_id');
    $webhook_secret = $this->config->get('webhook_secret');
    $expected_token = Crypt::hmacBase64($event_id, $webhook_secret);
    if (hash_equals($expected_token, $token)) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }

    return $result;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function page(Request $request) {
    $response = new Response();
    $response-> setPrivate();
    $response->headers->set('Content-type', 'application/json');

    $data = $request->getContent();
    if ($data) {
      // @todo - refactor other code to OO.
      eventbrite_attendees_sync_new();
      $content = json_encode(['success' => TRUE]);
      $response->setContent($content);
      $decoded = json_decode($data);
      // Don't log invalid data which could be XSS or other garbage.
      $message = is_null($decoded) ? 'Invalid JSON' : print_r($decoded, TRUE);
      $this->logger->info($message);
    }

    return $response;
  }

}
