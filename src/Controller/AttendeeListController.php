<?php

namespace Drupal\eventbrite_attendees\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AttendeeListController.
 */
class AttendeeListController extends ControllerBase {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * AttendeeListController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->config = $config_factory->get('eventbrite_attendees.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * @return array
   */
  public function page() {
    // Flag for when we want to add RDFa
    $rdf = function_exists('rdf_get_namespaces');
    $event_id = $this->config->get('event_id');
    $data = eventbrite_attendees_load($event_id);
    $header = array(t('Name'), t('Company'), t('Website'));
    $sponsor_rows = array();
    $rows = array();
    $opt_out_id = $this->config->get('opt_out_id');
    $username_id = $this->config->get('username_id');
    if ($username_id) {
      $header[] = $this->t('Drupal.org username');
    }
    $sponsor_ticket_id = $this->config->get('sponsor_ticket_id');
    $total_attendees = 0;
    $listed_attendees = 0;
    foreach ($data as $attendee) {
      $opt_out = FALSE;
      $username = '';
      if (is_array($attendee->answers)) {
        foreach ($attendee->answers as $ans) {
          if ($ans->question_id == $opt_out_id && !empty($ans->answer)) {
            $opt_out = TRUE;
          }
          if ($ans->question_id == $username_id && !empty($ans->answer)) {
            $username = $ans->answer;
          }
        }
      }
      $total_attendees++;
      if ($opt_out) {
        continue;
      }
      $listed_attendees++;
      $row = array('data' => array());
      if ($rdf) {
        $row['typeof'] = "schema:Person";
      }
      $cell = array('data' => $attendee->first_name . ' ' . $attendee->last_name);
      if ($rdf) {
        $cell['property'] = "schema:name";
      }
      $row['data'][] = $cell;
      $cell = array();
      if (!empty($attendee->company)) {
        $cell['data'] = $attendee->company;
        if ($rdf) {
          $cell['property'] = "schema:affiliation";
        }
      }
      $row['data'][] = $cell;
      $cell = array();
      if (!empty($attendee->website)) {
        $attributes = array();
        if ($rdf) {
          $attributes['rel'] = "schema:url";
        }
        // Make sure the website URL is properly formed.
        preg_match('@(https?://){0,1}(.+)@', $attendee->website, $m);
        if (!$m[1]) {
          $m[1] = 'http://';
        }
        $cell['data'] = \Drupal::l(rtrim($m[2], '/'), Url::fromUri($m[1] . $m[2], array('attributes' => $attributes)));
      }
      $row['data'][] = $cell;
      if ($username_id) {
        $cell = array();
        $cell['data'] = $username;
        if ($rdf && $username) {
          $cell['property'] = "foaf:nick";
        }
        $row['data'][] = $cell;
      }
      if ($sponsor_ticket_id && $sponsor_ticket_id == $attendee->ticket_class_id) {
        $sponsor_rows[] = $row;
      }
      else {
        $rows[] = $row;
      }
    }
    if ($sponsor_rows) {
      $output['sponsor_rows'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $sponsor_rows,
        '#caption' => $this->t('Individual Sponsor attendees')
      ];
    }
    $output['regular_rows'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#caption' => $this->t('Regular Attendees')
    ];
    $output['totals'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#markup' => t('@total registered attendees (@listed listed)', array('@total' => $total_attendees, '@listed' => $listed_attendees)),
      '#suffix' => '</p>',
    ];
    // If the config changes the page content needs to be invalidated.
    $meta = CacheableMetadata::createFromObject($this->config);
    $meta->addCacheTags(['eventbrite_attendees:list']);
    $meta->applyTo($output);

    return $output;
  }

}
