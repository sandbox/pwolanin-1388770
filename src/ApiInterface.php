<?php

namespace Drupal\eventbrite_attendees;

/**
 * Interface ApiInterface.
 */
interface ApiInterface {

  /**
   * Fetch data from Eventbrite.
   *
   * @param string $changed_since
   *   Optional date and time in ISO-like format like '2013-01-28 00:00:00Z'.
   *   Needs to use UTC time. Only attendees modified on or
   *   after this date/time will be returned.
   * @param string $status
   *   Valid values are 'attending', 'not_attending' and 'unpaid'.
   *
   * @return AttendeeList
   *   API result object decoded from JSON or NULL on error.
   */
  public function attendeesFetch($changed_since = '', $status = 'attending');

}
