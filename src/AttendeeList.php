<?php
/**
 * @file
 * Contains \Drupal\eventbrite_attendees\AttendeeList.
 */

namespace Drupal\eventbrite_attendees;

/**
 * Class AttendeeList
 * @package Drupal\eventbrite_attendees
 */
class AttendeeList implements \Countable, \Iterator {

  /**
   * @var int
   */
  private $position = 0;

  /**
   * @var array
   */
  protected $list = [];

  /**
   * @var string
   */
  protected $status;

  /**
   * The fields we care about for DB insert.
   *
   * @var array
   */
  protected static $fields = [
    'id',
    'event_id',
    'ticket_class_id',
    'order_id',
    'amount_paid',
    'currency',
    'promotional_code',
    'email',
    'first_name',
    'last_name',
    'company',
    'website',
    'created',
    'changed',
  ];

  /**
   * AttendeeList constructor.
   * @param array $list
   * @param $status
   */
  function __construct(array $list, $status) {
    $this->list = array_values($list);
    $this->status = $status;
  }

  /**
   * @return array
   */
  public static function fields() {
    return static::$fields;
  }

  /**
   * Transform raw JSON data into normalized list.
   *
   * @param array $data
   * @param string $status
   * @return \Drupal\eventbrite_attendees\AttendeeList
   */
  public static function fromJsonData($data, $status) {
    $list = [];
    foreach ($data as $attendee) {
      $normalized = new \stdClass();
      $answers = is_array($attendee->answers) ? $attendee->answers : [];
      $normalized->answers = [];
      foreach ($answers as $ans) {
        if (!isset($ans->answer)) {
          $ans->answer = '';
        }
        $normalized->answers[$ans->question_id] = $ans;
      }
      foreach (static::fields() as $key) {
        $normalized->{$key} = isset($attendee->{$key}) ? $attendee->{$key} : '';
      }
      // Eventbrite sucks - responds with number of "minor" units of currency
      // which is cents for dollars. Most world currencies shift 2
      // places so hard code that for now.  See ISO 4217.
      $normalized->amount_paid = $attendee->costs->gross->value / 100.0;
      $normalized->currency = $attendee->costs->gross->currency;
      $normalized->promotional_code = isset($attendee->promotional_code->code) ? $attendee->promotional_code->code : '';
      $normalized->email = $attendee->profile->email;
      $normalized->first_name = $attendee->profile->first_name;
      $normalized->last_name = $attendee->profile->last_name;
      $normalized->company = isset($attendee->profile->company) ? $attendee->profile->company : '';
      $normalized->website = isset($attendee->profile->website) ? $attendee->profile->website : '';
      $list[] = $normalized;
    }
    return new static($list, $status);
  }

  /**
   * Implements \Iterator::rewind()
   */
  function rewind() {
    $this->position = 0;
  }

  /**
   * Implements Iterator::current()
   *
   * @return object
   *   Data for one attendee.
   */
  function current() {
    return $this->list[$this->position];
  }

  /**
   * Implements \Iterator::key()
   *
   * @return int
   */
  function key() {
    return $this->position;
  }

  /**
   * Implements \Iterator::next()
   */
  function next() {
    ++$this->position;
  }

  /**
   * Implements \Iterator::valid()
   *
   * @return bool
   */
  function valid() {
    return isset($this->list[$this->position]);
  }

  /**
   * @return int
   */
  public function count() {
    return count($this->list);
  }

  public function status() {
    return $this->status;
  }
}

