<?php

namespace Drupal\eventbrite_attendees;

use GuzzleHttp\Client;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Exception\ClientException;

/**
 * Class Api.
 */
class Api implements ApiInterface {

  const BASE_URL = 'https://www.eventbriteapi.com/v3';

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Event ID.
   *
   * @var string
   */
  protected $eventId;

  /**
   * Oauth bearer token.
   *
   * @var string
   */
  protected $oauthToken = '';

  /**
   * Constructs a new Api object.
   */
  public function __construct(Client $http_client, ConfigFactory $config_factory) {
    $this->httpClient = $http_client;
    $config = $config_factory->get('eventbrite_attendees.settings');
    $this->eventId = (string) $config->get('event_id');
    $this->oauthToken = (string) $config->get('oauth_token');
  }

  /**
   * @inheritdoc
   */
  public function attendeesFetch($changed_since = '', $status = 'attending') {
    $query = [
      'status' => $status,
      'expand' => 'promotional_code',
    ];
    if ($changed_since) {
      $query['changed_since'] = $changed_since;
    }
    $data = $this->getAll("attendees", '/events/' . $this->eventId . '/attendees/', $query);
    return AttendeeList::fromJsonData($data, $status);
  }

  /**
   * Get all the data if the data is paged.
   *
   * @param string $key
   *   The expected key in the response containing the data of interest.
   * @param string $path
   *   The API endpoint path (with leading slash).
   * @param array $query
   *   Query string parameters in array form.
   *
   * @return mixed
   *
   * @throws \InvalidArgumentException if the JSON cannot be decoded.
   * @throws \GuzzleHttp\Exception\GuzzleException if there is an HTTP error.
   * @throws \RuntimeException if the response code is not 200.
   */
  protected function getAll($key, $path, $query = []) {
    $return = [];
    $headers = [
      'Authorization' => 'Bearer ' . $this->oauthToken,
    ];
    $options = [
      'headers' => $headers,
      'query' => $query,
    ];
    $url = static::BASE_URL . $path;
    do {
      $response = $this->httpClient->get($url, $options);
      if ($response->getStatusCode() != 200) {
        throw new \RuntimeException($response->getReasonPhrase());
      }
      $body = $response->getBody();
      $data = $this->jsonDecode($body);
      $pagination = $data->pagination;
      $continuation = isset($pagination->continuation) ? $pagination->continuation : NULL;
      $options['query']['continuation'] = $continuation;
      if (isset($data->{$key})) {
        $return = array_merge($return, $data->{$key});
      }
    } while ($continuation);
    return $return;
  }

  /**
   * Wrapper for json_decode that throws when an error occurs.
   *
   * Copied from \GuzzleHttp\json_decode().
   *
   * @param string $json    JSON data to parse
   * @param bool $assoc     When true, returned objects will be converted
   *                        into associative arrays.
   * @param int    $depth   User specified recursion depth.
   * @param int    $options Bitmask of JSON decode options.
   *
   * @return mixed
   * @throws \InvalidArgumentException if the JSON cannot be decoded.
   * @link http://www.php.net/manual/en/function.json-decode.php
   */
  protected function jsonDecode($json, $assoc = false, $depth = 512, $options = 0) {
    $data = \json_decode($json, $assoc, $depth, $options);
    if (JSON_ERROR_NONE !== json_last_error()) {
      throw new \InvalidArgumentException('json_decode error: ' . json_last_error_msg());
    }

    return $data;
  }

}
