<?php

namespace Drupal\eventbrite_attendees\Form;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eventbrite_attendees_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eventbrite_attendees.settings');

    $form['oauth_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Personal OAuth Token'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_token'),
      '#required' => TRUE,
    ];
    $form['event_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Eventbrite Event Id'),
      '#description' => $this->t('The Eventbrite event ID for the event whose attendees you want to list.'),
      '#maxlength' => 64,
      '#size' => 20,
      '#default_value' => $config->get('event_id'),
      '#required' => TRUE,
    ];
    $form['additional'] = array(
      '#type' => 'details',
      '#title' => $this->t('Additional configuration'),
    );
    $form['additional']['opt_out_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Eventbrite Opt Out Question Id'),
      '#default_value' => $config->get('opt_out_id'),
      '#description' => $this->t('The Eventbrite question ID for the checkbox where the user may opt out of being listed.'),
    );
    $form['additional']['username_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Eventbrite Drupal.org username Question Id'),
      '#default_value' => $config->get('username_id'),
      '#description' => $this->t('The Eventbrite question ID where attendess may enter their drupal.org username.'),
    );
    $form['additional']['sponsor_ticket_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Eventbrite Individual Sponsor Ticket Id'),
      '#default_value' => $config->get('sponsor_ticket_id'),
      '#description' => $this->t('The Eventbrite ticket ID purchased by individual sponsors.'),
    );
    $form['webhook'] = array(
      '#type' => 'details',
      '#title' => $this->t('Webhook configuration'),
    );
    $webhook_secret = $config->get('webhook_secret');
    if ($webhook_secret) {
      $token = Crypt::hmacBase64($config->get('event_id'), $webhook_secret);
      $url = Url::fromRoute('eventbrite_attendees.webhook', ['token' => $token], ['absolute' => TRUE])->toString();
      $message = $this->t('Webhook URL: :url', [':url' => $url]);
      $form['webhook']['webhook_secret'] = [
        '#type' => 'value',
        '#value' => $webhook_secret,
      ];
    }
    else {
      $message = $this->t('Submit the setting form to generate the webhook URL for the current Eventbrite Event Id');
    }
    $form['webhook']['webhook_url'] = [
      '#type' => 'markup',
      '#markup' => $message,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('eventbrite_attendees.settings');
    $config->setData([]);
    // Save result.
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    // This is not an important secret, it just protects the webhook URL
    // from crawlers so putting it in config is not a risk.
    if (!$config->get('webhook_secret')) {
      $secret = Crypt::randomBytesBase64();
      $config->set('webhook_secret', $secret);
    }
    $config->save();
    \Drupal::state()->set('eventbrite_attendees:last_fetch', '');
    parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['eventbrite_attendees.settings'];
  }

}
